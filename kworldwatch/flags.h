#ifndef __FLAGS_H__
#define __FLAGS_H__


#include <qstring.h>
#include <qcolor.h>
#include <qlist.h>
#include <qpoint.h>
#include <qpainter.h>
#include <qpixmap.h>
#include <qbitmap.h>


#include <kconfig.h>


class Flag
{
public:
  
  Flag(double lo, double la, QColor col)
    : _lo(lo), _la(la), _col(col) {};

  double longitude() { return _lo; };
  double latitude() { return _la; };

  QColor color() { return _col; };

  QString annotation() { return _ann; };
  void setAnnotation(QString ann) { _ann = ann; };


private:

  double _lo, _la;

  QColor _col;

  QString _ann;

};


class FlagList
{
public:

  FlagList();

  void paint(QPainter *p, int w, int h, int offset);

  void addFlag(Flag *f);

  void removeNearestFlag(QPoint target, int w, int h, int offset);

  void save(KConfig *config);
  void load(KConfig *config);
  

private:
 
  QPoint getPosition(double la, double lo, int w, int h, int offset); 

  QList<Flag> _flags;

  QPixmap _flagPixmap;
  QBitmap _flagMask;

};


#endif
