#ifndef __ZONECLOCK_H__
#define __ZONECLOCK_H__


#include <qwidget.h>
#include <qstring.h>
#include <qframe.h>
#include <qlist.h>


class QLabel;
class SimpleFlow;
class KConfig;


class ClockDialog;


class ZoneClock : public QFrame
{
  Q_OBJECT

public:

  ZoneClock(QString zone, QString name, QWidget *parent=0, const char *n=0);


  QString zone() const { return _zone; };
  void setZone(QString z) { _zone = z; updateTime(); };

  QString name() const { return _name; };
  void setName(QString n) { _name = n; updateTime(); };


signals:

  void removeMe(ZoneClock *t);
  void addClock(QString zone);
  void changed();


public slots:

  void updateTime();


protected:

  virtual bool eventFilter(QObject *, QEvent *);


private slots:

  void editClock();
  void slotRemoveClock();
  void removeTimeout();
  void slotAddClock();


private:

  QString _zone;
  QString _name;
  QLabel  *_timeLabel, *_nameLabel;
  QPopupMenu *_popup;

};


class ZoneClockPanel : public QFrame
{
  Q_OBJECT

public:

  ZoneClockPanel(QWidget *parent=0, const char *name=0);

  void addClock(QString zone, QString name);

  void save(KConfig *config);
  void load(KConfig *config);


public slots:

  void addClock(QString zone);


private slots:

  void updateTimer();
  void realign();
  void removeClock(ZoneClock *);


private:

  void createDialog();

  SimpleFlow *_flow;  
  QList<ZoneClock> _clocks;
  ClockDialog *_dlg;

};


#endif
