
#include "config.h"

#include <stdlib.h>
#include <time.h>
#include <iostream.h>


#include <qlabel.h>
#include <qlayout.h>
#include <qdatetime.h>
#include <qtimer.h>
#include <qcombobox.h>
#include <qlineedit.h>
#include <qpopupmenu.h>


#include <kglobal.h>
#include <klocale.h>
#include <kconfig.h>


#include "flow.h"
#include "clock.h"
#include "cities.h"
#include "zoneclock.moc"


ZoneClock::ZoneClock(QString zone, QString name, QWidget *parent, const char *n)
  : QFrame(parent, n), _zone(zone), _name(name)
{
  setFrameStyle(QFrame::Panel | QFrame::Raised);

  QHBoxLayout *hbox = new QHBoxLayout(this, 2,2);
  
  _name.append(":");
  _nameLabel = new QLabel(_name, this);
  hbox->addWidget(_nameLabel, 1);
  hbox->addSpacing(4);

  _timeLabel = new QLabel(this);
  hbox->addWidget(_timeLabel, 0, Qt::AlignRight);

  _popup = new QPopupMenu(this);
  _popup->insertItem(i18n("&Edit..."), this, SLOT(editClock()));
  _popup->insertItem(i18n("&Add..."), this, SLOT(slotAddClock()));
  _popup->insertItem(i18n("&Remove"), this, SLOT(slotRemoveClock()));

  _nameLabel->installEventFilter(this);
  _timeLabel->installEventFilter(this);

  updateTime();
}


void ZoneClock::slotRemoveClock()
{
  // Note: this is stupid, but we can't get us deleted
  // from this slot, as we would return and crash.
  // So instead we fire up an idle event triggering the delete
  // after the return.

  QTimer::singleShot(0, this, SLOT(removeTimeout()));
}


void ZoneClock::removeTimeout()
{
  emit removeMe(this);
}


void ZoneClock::slotAddClock()
{
  emit addClock(_zone);
}


void ZoneClock::editClock()
{
  ClockDialog *_dlg = new ClockDialog(this, 0, true);
  CityList cities;
  _dlg->ClockZone->insertStringList(cities.timezones());

  _dlg->ClockCaption->setText(_name.left(_name.length()-1));
  for (int i=0; i<_dlg->ClockZone->count(); ++i)
    if (_dlg->ClockZone->text(i) == _zone)
      {
        _dlg->ClockZone->setCurrentItem(i);
        break;
      }

  if (_dlg->exec() == QDialog::Accepted)
    {
      _name = _dlg->ClockCaption->text()+":";
      _nameLabel->setText(_name);
      _zone = _dlg->ClockZone->currentText();
      updateTime();
      layout()->invalidate();
      emit changed();
    }  

  delete _dlg;
}


bool ZoneClock::eventFilter(QObject *obj, QEvent *ev)
{
  if (ev->type() == QEvent::MouseButtonPress)
    {
      QMouseEvent *e = (QMouseEvent*)ev;
      if (e->button() == QMouseEvent::RightButton)
	_popup->exec(e->globalPos());      
    }

  return QFrame::eventFilter(obj, ev);
} 


void ZoneClock::updateTime()
{
  char *initial_TZ = getenv("TZ");
  setenv("TZ", _zone.latin1(), 1);
 
  time_t t = time(NULL);
  QDateTime dt;
  dt.setTime_t(t);
  _timeLabel->setText(QString("%1, %2").arg(KGlobal::locale()->formatTime(dt.time(), true)).arg(KGlobal::locale()->formatDate(dt.date(), true)));
 
  if (initial_TZ != 0) setenv("TZ", initial_TZ, 1);
}


ZoneClockPanel::ZoneClockPanel(QWidget *parent, const char *name)
  : QFrame(parent, name), _dlg(0)
{
  _flow = new SimpleFlow(this,1,1);

  QTimer *t = new QTimer(this);

  connect(t, SIGNAL(timeout()), this, SLOT(updateTimer()));
  t->start(500);

  _clocks.setAutoDelete(true);
}


void ZoneClockPanel::createDialog()
{
  if (!_dlg)
    {
      _dlg = new ClockDialog(this, 0, true);
      CityList cities;
      _dlg->ClockZone->insertStringList(cities.timezones());
    }
}


void ZoneClockPanel::addClock(QString zone, QString name)
{
  // add the clocks
  ZoneClock *zc = new ZoneClock(zone, name, this);
  _flow->add(zc);
  _clocks.append(zc);
  zc->show();
 
  realign();

  connect(zc, SIGNAL(addClock(QString)), this, SLOT(addClock(QString)));
  connect(zc, SIGNAL(changed()), this, SLOT(realign()));
  connect(zc, SIGNAL(removeMe(ZoneClock *)), this, SLOT(removeClock(ZoneClock *)));
}


void ZoneClockPanel::removeClock(ZoneClock *clock)
{
  _clocks.remove(clock);
  realign();
}


void ZoneClockPanel::realign()
{
  // realign the labels
  int w = 0;
  QListIterator<ZoneClock> it(_clocks);
  for ( ; it.current(); ++it)
    if (it.current()->sizeHint().width() > w)
      w = it.current()->sizeHint().width();
  it.toFirst();
  for ( ; it.current(); ++it)
    it.current()->setFixedWidth(w);
}


void ZoneClockPanel::updateTimer()
{
  QListIterator<ZoneClock> it(_clocks);
  for ( ; it.current(); ++it)
    it.current()->updateTime();
}


void ZoneClockPanel::addClock(QString zone)
{
  createDialog();

  QString name = zone;
  int pos = name.find("/");
  if (pos >= 0)
    name = name.mid(pos+1);

  _dlg->ClockCaption->setText(name);
  for (int i=0; i<_dlg->ClockZone->count(); ++i)
    if (_dlg->ClockZone->text(i) == zone)
      {
        _dlg->ClockZone->setCurrentItem(i);
        break;
      }

  if (_dlg->exec() == QDialog::Accepted)
    {
      addClock(_dlg->ClockZone->currentText(), _dlg->ClockCaption->text());
      update();
    }
}


void ZoneClockPanel::save(KConfig *config)
{
  config->writeEntry("Clocks", _clocks.count());
 
  QListIterator<ZoneClock> it(_clocks);
  int cnt=0;
  for ( ; it.current(); ++it)
    {
      QString n = it.current()->name();
      n = n.left(n.length()-1);
      config->writeEntry(QString("Clock_%1_Name").arg(cnt), n);
      config->writeEntry(QString("Clock_%1_Zone").arg(cnt), it.current()->zone());
      cnt++;
    }  
}


void ZoneClockPanel::load(KConfig *config)
{
  _clocks.clear();

  int num = config->readNumEntry("Clocks", 0);
 
  for (int i=0; i<num; ++i)
    {
      addClock(config->readEntry(QString("Clock_%1_Zone").arg(i)), config->readEntry(QString("Clock_%1_Name").arg(i)));
    }  
}

