#ifndef __MAP_LOADER_H__
#define __MAP_LOADER_H__


#include <qpixmap.h>
#include <qbitmap.h>
#include <qlist.h>


class MapTheme
{
public:

  MapTheme() : _name(""), _tag(""), _id(0) {};
  MapTheme(QString name, QString tag) : _name(name), _tag(tag), _id(0) {};

  QString tag() { return _tag; };
  QString name() { return _name; };

  void setID(int i) { _id = i; };
  int ID() { return _id; };


private:
  
  QString _name, _tag;
  int _id;
  
};


class MapLoader
{
public:

  static QList<MapTheme> themes();

  void load(unsigned int width=400, QString theme = "depths", unsigned int height=0);

  QPixmap lightMap() { return _light; };
  QPixmap darkMap() { return _dark; };

  QBitmap darkMask(int width, int height);


private:

  QStringList maps(QString theme);

  QPixmap _light, _dark;

};


#endif
