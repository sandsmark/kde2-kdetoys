#include <stdlib.h>
#include <iostream.h>
#include <stdio.h>


#include <qlabel.h>
#include <qlayout.h>
#include <qwidget.h>
#include <qpixmap.h>
#include <qimage.h>


#include <kapp.h>
#include <kconfig.h>
#include <kaboutdata.h>
#include <kcmdlineargs.h>
#include <klocale.h>
#include <kimageio.h>
#include <kdebug.h>


#include "main.moc"
#include "mapwidget.h"
#include "zoneclock.h"


WorldWideWatch::WorldWideWatch(bool restore, QWidget *parent, const char *name)
  : KMainWindow(parent, name)
{
  QWidget *w = new QWidget(this);
  setCentralWidget(w);

  setPlainCaption(i18n("The KDE World Clock"));
  
  QVBoxLayout *vbox = new QVBoxLayout(w, 0,0);
  
  _map = new MapWidget(false, restore, w);
  vbox->addWidget(_map,1);

  _clocks = new ZoneClockPanel(w);
  vbox->addWidget(_clocks);

  connect(_map, SIGNAL(addClockClicked(QString)), _clocks, SLOT(addClock(QString)));
  connect(_map, SIGNAL(saveSettings()), this, SLOT(doSave()));

  if (restore)
    load(kapp->config());
}


void WorldWideWatch::load(KConfig *config)
{
  _map->load(config);
  _clocks->load(config);

  resize(config->readNumEntry("Width", 320),
  config->readNumEntry("Height", 200));
}


void WorldWideWatch::save(KConfig *config)
{
  _map->save(config);
  _clocks->save(config);
  
  config->writeEntry("Width", width());
  config->writeEntry("Height", height());
}


void WorldWideWatch::doSave()
{
  save(kapp->config());
}


void WatchApplication::dumpMap()
{
  // guess some default parameters
  QSize mapSize(kapp->desktop()->size());
 
  KCmdLineArgs *args = KCmdLineArgs::parsedArgs();
 
  QCString themeName = args->getOption("theme");
  QCString outName = args->getOption("o");
 
  QCString ssize = args->getOption("size");
  if (!ssize.isEmpty())
    {
      int w,h;
      if (sscanf(ssize.data(), "%dx%d", &w, &h) == 2)
        mapSize = QSize(w,h);
    }
 
  kdDebug() << "theme=" << themeName << " out=" << outName << " size=" << mapSize.width() << "x" << mapSize.height() << endl;

  MapWidget *w = new MapWidget(false, true, 0);
  w->resize(mapSize);
  w->setTheme(themeName);

  QPixmap p = QPixmap::grabWidget(w, 0,0, mapSize.width(), mapSize.height());
  p.save(outName, "PPM");

  delete w;
}


int WatchApplication::newInstance()
{
  // dump mode, used for background drawing
  KCmdLineArgs *args = KCmdLineArgs::parsedArgs();
  if (args->isSet("dump"))
    {
      dumpMap();
      return 0;
    }

  WorldWideWatch *www = new WorldWideWatch(true);
  www->show();

  return 0;
}


static KCmdLineOptions options[] =
{
  { "dump", I18N_NOOP("Write out a file containing the actual map"), 0 },
  { "theme <file>", I18N_NOOP("The name of the theme to use for --dump"), "depths"},
  { "o <file>", I18N_NOOP("The name of the file to write to"), "dump.ppm" },
  { "size <WxH>", I18N_NOOP("The size of the map to dump"), 0 },
  { 0, 0, 0 }
};


int main(int argc, char *argv[])
{
  KAboutData about("kworldclock", I18N_NOOP("KDE World Clock"), "1.5");
  KCmdLineArgs::init(argc, argv, &about);
  KCmdLineArgs::addCmdLineOptions(options);
  KUniqueApplication::addCmdLineOptions();

  if (!KUniqueApplication::start())
    exit(0);

  WatchApplication app;

  if (app.isRestored())
    RESTORE(WorldWideWatch)

  return app.exec();
}
