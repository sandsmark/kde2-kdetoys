#ifndef __CITIES_H__
#define __CITIES_H__


#include <qstring.h>
#include <qstringlist.h>
#include <qlist.h>


class QPainter;


class City
{
public:

  City(QString n, double la, double lo) : _name(n), _latitude(la), _longitude(lo) {};
  QString name() { return _name; };

  double latitude() { return _latitude; };
  double longitude() { return _longitude; };


private:

  QString _name;
  double _latitude, _longitude;

};


class CityList
{
public:

  CityList();
  ~CityList();
  void paint(QPainter *p, int width, int height, int offset);
  
  City *getNearestCity(int w, int h, int offset, int x, int y, QPoint &where);

  QStringList timezones();

  
private:

  void readCityLists();
  void readCityList(QString fname);

  QPoint getPosition(double la, double lo, int w, int h, int offset);


private:

  QList<City> _cities;

};


#endif
