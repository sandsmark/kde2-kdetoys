#ifndef __KWW_applet_h__
#define __KWW_applet_h__


#include <qstring.h>
#include <qpixmap.h>


#include <kpanelapplet.h>


class MapWidget;


class KWWApplet : public KPanelApplet
{
  Q_OBJECT
    
public:

  KWWApplet(const QString& configFile, Type t = Normal, int actions = 0,
     QWidget *parent = 0, const char *name = 0);
  ~KWWApplet();

  int widthForHeight(int height) const;
  int heightForWidth(int width) const;


protected:

  void mousePressEvent(QMouseEvent *ev);


private:

  MapWidget *map;


};


#endif
