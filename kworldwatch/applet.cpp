#include <qlayout.h>


#include <kapp.h>
#include <kglobal.h>
#include <klocale.h>
#include <kiconloader.h>


#include "mapwidget.h"
#include "applet.moc"


extern "C"
{
  KPanelApplet *init(QWidget *parent, const QString& configFile)
  {
    KGlobal::locale()->insertCatalogue("kworldclock");
    return new KWWApplet(configFile, KPanelApplet::Normal,
			 KPanelApplet::About | KPanelApplet::Help | KPanelApplet::Preferences,
			 parent, "kwwapplet");
  }
}


KWWApplet::KWWApplet(const QString& configFile, Type type, int actions,
		     QWidget *parent, const char *name)
  : KPanelApplet(configFile, type, actions, parent, name)
{
  // make use of the icons installed for ksaferppp
  KGlobal::iconLoader()->addAppDir("kworldclock");

  QVBoxLayout *vbox = new QVBoxLayout(this, 0,0);
  
  map = new MapWidget(true, true, this);
  map->load(config());
  vbox->addWidget(map);
}


KWWApplet::~KWWApplet()
{
  map->save(config());
}


int KWWApplet::widthForHeight(int height) const
{
  return height*2;
}


int KWWApplet::heightForWidth(int width) const
{
  return width/2;
}


void KWWApplet::mousePressEvent(QMouseEvent *ev)
{
}
