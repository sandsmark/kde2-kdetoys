#include <kuniqueapp.h>
#include <kmainwindow.h>


class MapWidget;
class ZoneClockPanel;


class WorldWideWatch : public KMainWindow
{
  Q_OBJECT

public:

  WorldWideWatch(bool restore=false, QWidget *parent=0, const char *name=0);

  void save(KConfig *config);
  void load(KConfig *load);


protected:

  void readProperties(KConfig *sc) { load(sc); };
  void saveProperties(KConfig *sc) { save(sc); };


private slots:

  void doSave();


private:

  MapWidget *_map;
  ZoneClockPanel *_clocks;
  
};


class WatchApplication : public KUniqueApplication
{
  Q_OBJECT

public:

  WatchApplication() : KUniqueApplication() {};

  int newInstance();

  void dumpMap();

};


