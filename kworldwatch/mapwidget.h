#ifndef __MAP_WIDGET_H__
#define __MAP_WIDGET_H__


#include <time.h>


#include <qwidget.h>
#include <qpixmap.h>
#include <qstringlist.h>
#include <qlabel.h>
#include <qpoint.h>


class QPopupMenu;


#include <kconfig.h>


class CityList;
class FlagList;


#include "maploader.h"


class MapWidget : public QWidget
{
  Q_OBJECT

public:

  MapWidget(bool applet=false, bool restore=false, QWidget *parent=0, const char *name=0);
  ~MapWidget();

  void setTheme(QString theme);
  void setTime(struct tm *time);
  void setIllumination(bool i);
  void setCities(bool c);
  void setFlags(bool f);

  void save(KConfig *config);
  void load(KConfig *config);

  void updateBackground();

  QPixmap getPixmap();


signals:

  void addClockClicked(QString zone);
  void saveSettings();


protected slots:

  void timeout();


public slots:

  void about();

  void toggleIllumination();
  void toggleCities();
  void toggleFlags();

  void removeFlag();

  void slotSaveSettings();

	  
protected:

  void resizeEvent(QResizeEvent *ev);
  void paintEvent(QPaintEvent *ev);
  void mousePressEvent(QMouseEvent *ev);
  void mouseMoveEvent(QMouseEvent *ev);
  void enterEvent(QEvent *ev);
  void leaveEvent(QEvent *ev);

  QPixmap calculatePixmap();


private slots:

  void themeSelected(int index);
  void addFlag(int index);
  void addClock();


private:

  void updateMap();
  QString cityTime(QString city);
  void showIndicator(QPoint pos);

  MapLoader _loader;

  QString _theme;

  QPixmap _pixmap;

  int gmt_position;

  time_t sec;

  QPopupMenu *_popup, *_themePopup, *_flagPopup;
  QList<MapTheme> _themes;

  bool _illumination, _cities, _flags;
  int _illuminationID, _citiesID, _flagsID;

  CityList *_cityList;
  QLabel *_cityIndicator;
  QString _currentCity;

  FlagList *_flagList;
  QPoint _flagPos;
 
  bool _applet;

};


#endif
