add_subdirectory(data)

qt2_wrap_cpp(amor_MOC
    SOURCES
        amorbubble.h
        amordialog.h
        amor.h
        amorwidget.h
)

kde2_kidl(amor_IDL
    SOURCES
        AmorIface.h
)


kde2_kinit_executable(amor2
    SOURCES
        amordialog.cpp
        amor.cpp
        amorwidget.cpp
        main.cpp
        amoranim.cpp
        amorpm.cpp
        amorbubble.cpp
        amorconfig.cpp
        amortips.cpp

        AmorIface_skel.cpp
        ${amor_IDL}

        ${amor_MOC}
    LIBS
        kde2::kdeui
)

install(FILES amor.desktop DESTINATION ${KDE2_APPSDIR}/Toys)
kde2_stdicon()
