<?xml version="1.0" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.1-Based Variant V1.0//EN" "dtd/kdex.dtd" [
 <!ENTITY kappname "&kodo;">
 <!ENTITY % addindex "IGNORE">
 <!ENTITY % English "INCLUDE">
]>

<book lang="&language;">
<bookinfo>
<title>The &kodo; Handbook</title>
<authorgroup>
<author>
<firstname>Armen</firstname>
<surname>Nakashian</surname>
<affiliation>
<address><email>armen@tourismo.com</email></address>
</affiliation>
</author>
<!-- TRANS:ROLES_OF_TRANSLATORS -->
</authorgroup>

<copyright>
<year>1999</year><year>2001</year>
<holder>Armen Nakashian</holder>
</copyright>

<legalnotice>&FDLNotice;</legalnotice>

<date>2001-03-23</date>
<releaseinfo>2.01.00</releaseinfo>

<abstract>
<para>&kodo; is a little gadget to measure your desktop mileage</para>
</abstract>

<keywordset>
<keyword>KDE</keyword>
<keyword>KOdometer</keyword>
<keyword>odometer</keyword>
<keyword>mouse</keyword>
<keyword>mileage</keyword>
</keywordset>
</bookinfo>

<chapter id="introduction">
<title>Introduction</title>

<sect1 id="whats-kodo">
<title>What is &kodo;?</title>

<para>
&kodo; <emphasis>measures your desktop milage</emphasis>.  It tracks the
movement of your mouse pointer across your desktop, and renders it in
inches/feet/miles!  It can do cm/meters/km too.  The most exciting
feature is the tripometer, and its utter uselessness.</para>

<note>
<para>The next version will be USEFUL!</para>
</note>

</sect1>

</chapter>

<chapter id="onscreen-operation">
<title>Onscreen operation</title>

<screenshot>
<screeninfo>A labelled screenshot of &kodo; in action</screeninfo>
<mediaobject>
<imageobject>
<imagedata fileref="guide.png" format="PNG"/>
</imageobject>
<textobject>
<phrase>A labelled screenshot of &kodo; in action</phrase></textobject>
<caption><para>A screen shot. Note the fashionable lack of a
titlebar.</para></caption>
</mediaobject>
</screenshot>

<para>
The <guilabel>Odometer</guilabel> is the total distance traveled by the
mouse since the last odometer reset.  The
<guilabel>Tripometer</guilabel> is the distance traveled by the mouse
since:
</para>

<sect1 id="menu-options">
<title>Menu Options</title>

<para>
If you've gotten this far, you probably already know that there is a
&RMB; mouse button context menu on &kodo;, which contains a number of
choices.
</para>

<variablelist>
<varlistentry>
<term><guimenuitem>Enable</guimenuitem></term>
<listitem>
<para>
Toggle mileage tracking.  This is good if you want to cheat on your
mileage.  <emphasis>Note: This is illegal in most countries</emphasis>
</para>
</listitem>
</varlistentry>

<varlistentry>
<term><guimenuitem>Metric Display</guimenuitem></term>
<listitem>
<para>
Because &kodo; was written by an American, it obnoxiously assumes you
want to use Inches/Feet/Miles for measurement.  If you come from some
more modern country in Europe, to use metric measurements, turn on this
option.
</para>
</listitem>
</varlistentry>

<varlistentry>
<term><guimenuitem>Auto Reset Trip</guimenuitem></term>
<listitem>
<para>
This option will cause &kodo; to set the Tripometer back to zero every
time it starts up. </para>
</listitem>
</varlistentry>

<varlistentry>
<term><guimenuitem>Reset Trip</guimenuitem></term>
<listitem>
<para>
Set the tripometer to zero.
</para>
</listitem>
</varlistentry>

<varlistentry>
<term><guimenuitem>Reset Odometer</guimenuitem></term>
<listitem>
<para>
Set the odometer back to zero. <emphasis>You might get in trouble for
this!</emphasis></para>
</listitem>
</varlistentry>

<varlistentry>
<term><guimenuitem>About Mousepedometa</guimenuitem></term>
<listitem>
<para>
Pops up a little dialog box with information on who to blame for the
program.
</para>
</listitem>
</varlistentry>

<varlistentry>
<term><guimenuitem>Help</guimenuitem></term>
<listitem>
<para>
This menu entry opens up the &kodo; help - this very document. 
</para>
</listitem>
</varlistentry>

<varlistentry>
<term><guimenuitem>Quit</guimenuitem></term>
<listitem>
<para>
Quit &kodo;
</para>
</listitem>
</varlistentry>
</variablelist>

</sect1>

</chapter>

<chapter id="how-it-works">
<title>How It Works</title>

<para>&X-Windows; displays are usually configured with a screen-size
variable.  That is, how big your screen is in real-world measurements,
not pixels.  &kodo; grabs this value and uses it come up with a
&dpi; (dots per inch) value for your display, and thus
can relate the number of pixels you've traversed to the number of inches
the pointer appears to have moved on the screen.</para>

<para>But lets be real!  Its horribly <emphasis>inaccurate!</emphasis>
&kodo; makes no pretense of being even remotely accurate.  In fact,
someone has measured the movement of their mouse across the screen with
a ruler, and found that the distance algorithm is almost 25&percnt; off!
The original author's response to this complaint was <quote>who
cares?</quote>.</para>
</chapter>

<chapter id="license-and-credits">
<title>Credits and Licenses</title>

<para>&kodo; copyright 1998-2001, Armen Nakashian.</para>

<para>Documentation coypright 1998 Armen Nakashian, with small updates
and markup cleanup for &kde; 2.2, Lauri Watts
<email>lauri@kde.org</email>
</para>

&underFDL;
&underGPL;

</chapter>

<appendix id="installation">
<title>Installation</title>

<sect1 id="how-to-obtain-kodo">
<title>How to obtain &kodo;</title>

<para>&kodo; is a toy application of the &kde; project <ulink
url="http://www.kde.org">http://www.kde.org</ulink>.  It can be found on
<ulink
url="ftp://ftp.kde.org/pub/kde/">ftp://ftp.kde.org/pub/kde/</ulink>, the
main &FTP; site of the &kde; project, in the kdetoys snapshots. Please
consider using your nearest mirror &FTP; site to get the load away from
the server.</para>

</sect1>

<sect1 id="compilation-and-installation">
<title>Compilation and Installation</title>

<para>In order to compile and install &kodo; on your system, type the
following in the base directory of the &kodo; distribution:
</para>

<screen>
<prompt>%</prompt> <userinput><command>./configure</command></userinput>
<prompt>%</prompt> <userinput><command>make</command></userinput>
<prompt>%</prompt> <userinput><command>make</command> <option>install</option></userinput>
</screen>


<para>Since &kodo; uses <command>autoconf</command> you should have not
trouble compiling it. Should you run into problems, please report them
to my email address <email>armen@tourismo.com</email>.</para>
</sect1>

</appendix>

</book>

<!--
Local Variables:
mode: sgml
sgml-minimize-attributes:nil
sgml-general-insert-case:lower
sgml-indent-step:0
sgml-indent-data:nil
End:

// vim:ts=0:sw=2:tw=78:noet
-->
