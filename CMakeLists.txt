########### next target ###############

cmake_minimum_required(VERSION 3.1.0)
project(kde2-kdetoys
    VERSION 2.2.2
    )

find_package(Qt2 REQUIRED)
find_package(kdelibs2 REQUIRED)

find_package(ZLIB REQUIRED)
find_package(DCOP REQUIRED)
find_package(kdecore REQUIRED)
find_package(kdeui REQUIRED)
find_package(kssl REQUIRED)
find_package(kdesu REQUIRED)
find_package(kio REQUIRED)
find_package(ksycoca REQUIRED)
find_package(kscreensaver REQUIRED)

add_subdirectory(amor)
#add_subdirectory(eyesapplet)
#add_subdirectory(fifteenapplet)
#add_subdirectory(kmoon)
#add_subdirectory(kodo)
#add_subdirectory(kscore)
add_subdirectory(kteatime)
#add_subdirectory(ktux)
#add_subdirectory(kworldwatch)
