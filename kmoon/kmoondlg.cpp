/*
 *   kmoon - a moon phase indicator
 *   Copyright (C) 1998  Stephan Kulow
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include "kmoondlg.h"
#include <qslider.h>
#include <qlayout.h>
#include <qlabel.h>
#include <klocale.h>
#include "kmoon.h"
#include <qwhatsthis.h>
#include <qvbox.h>
#include <kapp.h>
#include <qpixmap.h>

KMoonDlg::KMoonDlg(int _angle, bool n, QWidget *parent, const char *name)
    : KDialogBase(parent, name, true, i18n("Change View"),
                  Ok|Cancel|Help), angle(_angle), north(n)
{
	QWidget *page = new QWidget( this );
	setMainWidget(page);
	QHBoxLayout *topLayout = new QHBoxLayout( page, 0, spacingHint() );

        QVBox *vbox = new QVBox(page);
        QHBox *hbox = new QHBox(vbox);
        hbox->setSpacing(15);

	QLabel *label = new QLabel( i18n("View Angle"), hbox, "caption" );
	QString text = i18n("This changes the angle the moon\n"
                            "is rotated to the position you see it\n"
                            "at the equator.\n"
                            "As this angle is (almost) impossible to\n"
                            "calculate from any system given data,\n"
                            "you can here configure how you want\n"
                            "kmoon to display your moon.\n"
                            "The default value is 0, but it's very\n"
                            "unlikely you see the moon outside this way.");
	QWhatsThis::add(label, text);

	slider = new QSlider( -90, 90, 2, angle, Qt::Horizontal, hbox, "slider" );
	slider->setTickmarks(QSlider::Above);
	slider->setTickInterval(45);
	slider->setEnabled(QPixmap::defaultDepth() > 8);
	label->setEnabled(QPixmap::defaultDepth() > 8);
	QWhatsThis::add(slider, text);
	connect(slider, SIGNAL(valueChanged(int)), SLOT(valueChanged(int)));

        QPushButton *hemitoggle = new QPushButton(vbox);
        hemitoggle->setText(i18n("Toggle Hemisphere"));
        connect(hemitoggle, SIGNAL(clicked()), SLOT(toggleHemi()));

        topLayout->addWidget(vbox);

	moon = new MoonWidget(page, "preview");
	moon->setMinimumSize(50, 50);
	moon->setMaximumSize(200,200);
	QWhatsThis::add(moon, i18n("The moon as kmoon would display it\n"
							   "following your current setting and time."));
	topLayout->addWidget(moon);
        connect(this, SIGNAL(helpClicked()), SLOT(help()));
	// disableResize();
}

void KMoonDlg::valueChanged(int value) {
    angle = value;
    moon->setAngle(value);
}

void KMoonDlg::help() {
    kapp->invokeHelp(QString::fromLatin1("config"));
}

void KMoonDlg::toggleHemi() {
    moon->setNorthHemi(!moon->northHemi());
    north = moon->northHemi();
}

#include "kmoondlg.moc"
