
#ifndef KIMGNUM_H
#define KIMGNUM_H

#include <qframe.h>
#include <qpixmap.h>

class KImageNumber : public QFrame
{
	Q_OBJECT
	Q_PROPERTY( double m_value READ value WRITE setValue )
public:
	KImageNumber(const QString& font, QWidget* parent=0, const char* name=0);

	virtual ~KImageNumber();

	void paintEvent(QPaintEvent*);
	virtual QSize sizeHint() const;
	double value() const;

public slots:
	void setValue(double v);

protected:
	double m_value;
	QPixmap* fontPix;
};

#endif
