/***************************************************************************
                          kscore_pref.h  -  description
                             -------------------
    begin                : Wed Aug 23 2000
    copyright            : (C) 2000 by Jonathan Singer
    email                : jsinger@leeta.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef PREF_DIALOG_H
#define PREF_DIALOG_H

#include <qdialog.h>
#include <qstring.h>
#include <qstringlist.h>
#include <qspinbox.h>
#include <qcolor.h>
class QVBoxLayout; 
class QHBoxLayout; 
class QGridLayout; 
class KComboBox;
class QLabel;
class QLineEdit;
class QPushButton;
class QSpinBox;
class KColorButton;


class pref_dialog : public QDialog
{ 
    Q_OBJECT

public:
    pref_dialog( QString , QString, QColor,  int ,
								QWidget* parent = 0, const char* name = 0, bool modal = FALSE,
 								WFlags fl = 0 );
	 ~pref_dialog();
    QLabel* League_label;
    KComboBox* League_box;
    QLabel* Team_label;
    QLineEdit* Team_box;
    QPushButton* Browse_button;
    QLabel* Color_label;
    KColorButton* Color_btn;
    QLabel* Time_label;
    QSpinBox* Time_box;
    QLabel* Time_label_2;
    QPushButton* OK_button;
    QPushButton* Cancel_button;

    QStringList Leagues;
    QString League;
    QString Team;
    int Interval;
public slots:
    void write( );
    void browseTeams(  );

signals:
	 void results( QString, QString, QColor, int);

protected:
    bool event( QEvent* );
};

#endif // PREF_DIALOG_H
