#!/usr/bin/perl

#---------------------------------------------------------------------
#
# This script downloads the appropriate web page from Excite and 
# parses the current (or final) score from the game the specified
# team (for the specified sport) is playing in.
#
# Modify this script as needed if you prefer another web site.
# Feel free to email your scripts to the maintainer.
# 
#---------------------------------------------------------------------


chomp($sport = <STDIN>);

chomp($team = <STDIN>);



$home       = ( getpwuid( $< ) )[7];
$dir        = "$home/.wmScoreBoard";
$sport_file = "$dir/scores.$sport.$team";
$team_file  = "$dir/score.$sport.$team";



# Excite's scores are in an HTML table so look for row markers

$/ = '</TR>';

#open ( HTML,  "<$sport_file" ) or exit 1;
while (1)
{
LINE: while (<STDIN>) 
{
   # Look for the date of the game

   if ( /SCORES\s*(&nbsp;)?([\w\s]+?)/ ) {

      # Mangle this row until it gets into a somewhat standard form

      s/\n//g;
      s/&nbsp;/ /g; 
      s/SCORES//g; 
      s/<.*?>//g;
      s/\s+/_/g;
      s/^_+//;
      $_ = uc( $_ );

      $date = $_;
      next LINE;
   }

   # Each team has a link so look for that link

   if ( /$sport\/$team/ ) {

      # Mangle this row until it gets into a somewhat standard form

      s/\n//g;                    # get rid of newlines
      s/&nbsp;/ /g;               # &nbsp should be real space
      s/<.*?>/#/g;                # remove HTML tags
      s/#\s+#/#/g;                # remove useless white space
      s/^[\s\d\#]*//;             # get rid of rankings
      s/\#+/#/g;                  # get rid of consecutive #'s
      s/#\s+(\w)/\#$1/g;          # get rid of whitespace at beginning
      s/(\w)\s+#/$1#/g;           # get rid of whitespace at end
      s/\s+/_/g;                  # get rid of consecutive whitespace
      s/#\s*\-\d+\.5\s*#/#_#_#/;  # get rid of odds
      s/#_*TV\:.*?_*#/#/;         # get rid of tv station

      # wmScoreBoard is expecting uppercase

      $_ = uc( $_ );

      # Finally, we can parse the info we need

      ( $visitors, $at, $home, $visitors_score, $home_score, $time, $period ) = split( "#", $_ );

      $period = $time . " " . $period;

      # I like 'FINAL' better

      $period =~ s/^_?F/FINAL/;

      # Check for error conditions

      if ( $home_score =~ /\:/ ) {
          $period         = $home_score;
          $home_score     = ' ';
          $visitors_score = ' ';
      }

      if ( $visitors_score =~ /\:/ ) {
          $period         = $visitors_score;
          $home_score     = ' ';
          $visitors_score = ' ';
      }

      if ( $home !~ /^[A-Z_]+$/ or $visitors !~ /^[A-Z_]+$/ ) {
         print  " \n \nNO SCORE\n \n \nAVAILABLE\n";
         exit;
      }
      
      # Print the info to the scores file in the form wmScoreBoard is expecting

      print "$home\n";
      print "$home_score\n";
      print "$visitors\n";
      print "$visitors_score\n";
      print "$period\n";
      print "$date\n";

      exit 0;
   }
}

print  " \n \nNO SCORE\n \n \nAVAILABLE\n";


exit 0;
}