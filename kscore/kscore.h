/***************************************************************************
                          kscore.h  -  description
                             -------------------
    begin                : Sat Aug  5 22:10:22 EDT 2000
    copyright            : (C) 2000 by Jonathan Singer
    email                : jsinger@leeta.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __example_h__
#define __example_h__

#include <qstring.h>
#include <kpanelapplet.h>

#include <qwidget.h>
#include <kpopupmenu.h>
#include <qstring.h>
#include <kprocess.h>
#include <kio/jobclasses.h>
#include <qtimer.h>
#include <klocale.h>
#include <khelpmenu.h>
#include <qcolor.h>

class KscoreApplet : public KPanelApplet
{
  Q_OBJECT
 public:
  KscoreApplet(const QString& configFile, Type t = Normal, int actions = 0,
                QWidget *parent = 0, const char *name = 0);

		int widthForHeight(int height) const;
		int heightForWidth(int width) const;

  public slots:
		void loadFile( KIO::Job *aJob );
		void readOutput( KProcess *, char *, int);
		void finishOutput( KProcess *);
		void changeSettings( QString, QString, QColor, int );
		void getData();

	protected:
		void paintEvent( QPaintEvent *);
		void mousePressEvent( QMouseEvent *event);

	private:
		bool 			horizontal;
		bool 			Online;
		bool			Downloading;
		KIO::FileCopyJob *theJob;
		KPopupMenu 	*menu;
		KHelpMenu 	*hmenu;
		QTimer 	  	Timer;
		int			Interval;


		KProcess 	proc;
		QString		Output;
		QString		Scoreboard;
		QString 		Status;
		QColor		Text_color;

		QString 		Team;
		QString		League;
		QString 		Search;
		QString 		PID;


		QString   	HomeName;
		QString   	VisitorsName;
		QString   	HomeScore;
		QString   	VisitorsScore;
		QString   	Period;
		QString   	Date;




};

#endif
