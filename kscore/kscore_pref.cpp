/***************************************************************************
                          kscore_pref.cpp  -  description
                             -------------------
    begin                : Wed Aug 23 2000
    copyright            : (C) 2000 by Jonathan Singer
    email                : jsinger@leeta.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software and can be used or redistributed        *
 *   subject to the included license.                                      *
 *                                                                         *
 ***************************************************************************/

#include "kscore_pref.h"

#include <kcombobox.h>
#include <qnamespace.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <kcombobox.h>
#include <kcolorbtn.h>
#include <qlayout.h>
#include <qvariant.h>
#include <qtooltip.h>
#include <klocale.h>
#include <iostream.h>
#include <krun.h>

/* 
 *  Constructs a pref_dialog which is a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f' 
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
pref_dialog::pref_dialog(  QString oldLeague, QString oldTeam, QColor oldColor,
      int oldInterval, QWidget* parent,  const char* name, bool modal, WFlags fl )
      : QDialog( parent, name, modal, fl )
{
    if ( !name )
		setName( "pref_dialog" );
    resize( 260, 224 );
    setCaption( i18n( "Kscore settings"  ) );

    League_label = new QLabel( this, "League_label" );
    League_label->setGeometry( QRect( 10, 9, 61, 20 ) );
    League_label->setText( i18n( "League"  ) );
    QFont League_label_font(  League_label->font() );
    League_label_font.setBold( TRUE );
    League_label->setFont( League_label_font );

    League_box = new KComboBox( FALSE, this, "League_box" );
    League_box->setGeometry( QRect( 80, 10, 160, 22 ) );
    QToolTip::add(  League_box, i18n( "Select a league" ) );

    Team_label = new QLabel( this, "Team_label" );
    Team_label->setGeometry( QRect( 10, 52, 61, 20 ) );
    Team_label->setText( i18n( "Team"  ) );
    QFont Team_label_font(  Team_label->font() );
    Team_label_font.setBold( TRUE );
    Team_label->setFont( Team_label_font );

    Team_box = new QLineEdit( this, "Team_box" );
    Team_box->setGeometry( QRect( 80, 52, 33, 22 ) );
    Team_box->setMaxLength( 3 );
    QToolTip::add(  Team_box, i18n( "Enter a team code" ) );

    Browse_button = new QPushButton( this, "Browse_button" );
    Browse_button->setGeometry( QRect( 140, 50, 100, 28 ) );
    Browse_button->setText( i18n( "Browse teams"  ) );
    QToolTip::add(  Browse_button, i18n( "Browse available team codes" ) );

    Color_btn = new KColorButton( oldColor, this, "Color_btn" );
    Color_btn->setGeometry( QRect( 80, 90, 160, 21 ) );

    Color_label = new QLabel( this, "Color_label" );
    Color_label->setGeometry( QRect( 10, 90, 61, 20 ) );
    Color_label->setText( i18n( "Text color"  ) );
    QFont Color_label_font(  Color_label->font() );
    Color_label_font.setBold( TRUE );
    Color_label->setFont( Color_label_font );

    Time_label = new QLabel( this, "Time_label" );
    Time_label->setGeometry( QRect( 10, 130, 90, 20 ) );
    Time_label->setText( i18n( "Update every"  ) );
    QFont Time_label_font(  Time_label->font() );
    Time_label_font.setBold( TRUE );
    Time_label->setFont( Time_label_font );
    Time_label->setScaledContents( FALSE );

    Time_box = new QSpinBox( this, "Time_box" );
    Time_box->setGeometry( QRect( 118, 130, 50, 21 ) );
    Time_box->setButtonSymbols( QSpinBox::PlusMinus );
    Time_box->setMinValue(  10 );
    Time_box->setMaxValue( 300 );

    Time_label_2 = new QLabel( this, "Time_label_2" );
    Time_label_2->setGeometry( QRect( 190, 130, 50, 20 ) );
    Time_label_2->setText( i18n( "minutes"  ) );
    QFont Time_label_2_font(  Time_label_2->font() );
    Time_label_2_font.setBold( TRUE );
    Time_label_2->setFont( Time_label_2_font );

    OK_button = new QPushButton( this, "OK_button" );
    OK_button->setGeometry( QRect( 20, 170, 80, 28 ) );
    OK_button->setText( i18n( "OK"  ) );

    Cancel_button = new QPushButton( this, "Cancel_button" );
    Cancel_button->setGeometry( QRect( 150, 170, 80, 28 ) );
    Cancel_button->setText( i18n( "Cancel"  ) );

    OK_button->setAccel(Key_Return);
    Cancel_button->setAccel(Key_Escape);

	 // tab order
    setTabOrder( League_box, Team_box );
    setTabOrder( Team_box, Browse_button );
    setTabOrder( Browse_button, (QWidget *)Color_btn );
    setTabOrder( (QWidget *)Color_btn, Time_box );
    setTabOrder( Time_box, OK_button );
    setTabOrder( OK_button, Cancel_button );


	 // Initialization
    League = oldLeague; Team = oldTeam; Interval = oldInterval;
//  cout << Interval << "minutes\n";
//  cout << "League is " << League.latin1() << "\n";
    Time_box->setValue( Interval );
    Team_box->setText( Team );
    Leagues << "NHL" << "nhl"; Leagues << "MLB" << "mlb";
    Leagues << "NFL" << "nfl"; Leagues << "NBA" << "nba";  Leagues << "WNBA" << "wnba";
    Leagues << "MLS" << "mls"; Leagues << "Bundesliga" << "bund";
    Leagues << "Dutch" << "dutch"; Leagues << "English Premier" << "premr";
    Leagues << "Spain Primera" << "span"; Leagues << "Italy Serie A" << "serie";
    Leagues << "NCAA Hockey" << "ncaah"; Leagues << "NCAA Football" << "ncaaf";
    Leagues << "NCAA M Basketball" << "ncaab"; Leagues << "NCAA W Basketball" << "ncaaw";
    Leagues << "NCAA Baseball" << "ncaad"; Leagues << "IHL" << "ihl";
    Leagues << "AHL" << "ahl"; Leagues << "ECHL" << "echl";
    Leagues << "CBA" << "cba"; Leagues << "CFL" << "cfl";
    Leagues << "Arena Football" << "arena"; Leagues << "NFL Europe" << "wlaf";
    int total_leagues = Leagues.count();
//  cout << total_leagues << " leagues in list\n";
    for ( int index =0; index < total_leagues; index +=2)
    		{
    		  League_box->insertItem( Leagues.operator[](index), (index/2));
    		  if (Leagues.operator[](index+1) == League)
    		  		League_box->setCurrentItem(index/2);
    		}



    // signals and slots connections
	 connect( Cancel_button, SIGNAL( clicked() ), this, SLOT( close() ));
    connect( OK_button, SIGNAL( clicked() ), this, SLOT( write() ));
    connect( Browse_button, SIGNAL( clicked() ), this, SLOT( browseTeams() ));
}		

/*  
 *  Destroys the object and frees any allocated resources
 */
pref_dialog::~pref_dialog()
{
    // no need to delete child widgets, Qt does it all for us
}

/*  
 *  Main event handler. Reimplemented to handle application
 *  font changes
 */

bool pref_dialog::event( QEvent* ev )
{
    bool ret = QDialog::event( ev );
    if ( ev->type() == QEvent::ApplicationFontChange ) {
	QFont Color_label_font(  Color_label->font() );
	Color_label_font.setBold( TRUE );
	Color_label->setFont( Color_label_font );
	QFont Team_label_font(  Team_label->font() );
	Team_label_font.setBold( TRUE );
	Team_label->setFont( Team_label_font );
	QFont League_label_font(  League_label->font() );
	League_label_font.setBold( TRUE );
	League_label->setFont( League_label_font );
	QFont Time_label_font(  Time_label->font() );
	Time_label_font.setBold( TRUE );
	Time_label->setFont( Time_label_font );
	QFont Time_label_2_font(  Time_label_2->font() );
	Time_label_2_font.setBold( TRUE );
	Time_label_2->setFont( Time_label_2_font );
    }
    return ret;
}


void pref_dialog::browseTeams(  )
{
	int index = League_box->currentItem();
	League = Leagues.operator[]((index*2)+1);
	QString Command = "konqueror http://sports.excite.com/" + League +"/";
	KRun::runCommand( Command, "konqueror", "konqueror" );
}

void pref_dialog::write()
{
	int index = League_box->currentItem();
	League = Leagues.operator[]((index*2)+1);
	Team = Team_box->text();
	Interval = Time_box->value();
	QColor newColor = Color_btn->color();
//	cout << "Writing " << " " << League.latin1() << " " << Team.latin1() << " "
//	<< Interval << "\n";
	emit results( League, Team, newColor, Interval );
	close();
}

#include "kscore_pref.moc"

